<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('customer_id')->nullable();
            $table->unsignedBigInteger('approve_by')->nullable();
            $table->string("name", 255)->nullable();
            $table->string("phone", 15)->nullable();
            $table->text('address')->nullable();
            $table->text('add_information')->nullable();
            $table->timestamp('pick_up')->nullable();
            $table->enum('status', [0, 1, 2, 3, 4])->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('orders');
    }
};
