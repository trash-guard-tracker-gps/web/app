<?php $__env->startComponent('mail::message'); ?>
# Forgot Password

You are receiving this email because you requested a password reset.

Your OTP (One-Time Password) is: **<?php echo e($otp); ?>**

This OTP will expire after a short period of time.

If you did not request a password reset, no further action is required.

Thanks,<br>
<?php echo e(config('app.name')); ?>

<?php echo $__env->renderComponent(); ?>
<?php /**PATH /home/escurtcheon/Documents/Code/trash_guard_gps_tracker/web/update-angga-1/app-main/resources/views/emails/forgot_password_mobile.blade.php ENDPATH**/ ?>