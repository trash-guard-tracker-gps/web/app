<?php $__env->startSection('title'); ?>
    Receive Order
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="card">
    <div class="card-header">
        <div class="card-title">
            Order
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table gy-5">
                <thead>
                    <tr class="fs-6 text-muted">
                        <th>No</th>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Address</th>
                        <th>Additional Information</th>
                        <th>Pickup At</th>
                        <th>Created At</th>
                        
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><?php echo e($index + 1); ?></td>
                        <td><?php echo e($order->name); ?></td>
                        <td><?php echo e($order->phone); ?></td>
                        <td><?php echo e($order->address); ?></td>
                        <td><?php echo e($order->add_information); ?></td>
                        <td><?php echo e($order->pick_up); ?></td>
                        <td><?php echo e($order->created_at); ?></td>
                        
                        <td class="d-flex flex-column flex-sm-row">
                            <?php if($order->status < 3): ?>
                            <form class="m-2" id="confirmForm<?php echo e($order->id); ?>" action="<?php echo e(route('order.update_status', ['orderId' => $order->id])); ?>" method="POST">
                                <?php echo csrf_field(); ?>
                                
                                <input type="hidden" name="status" value="<?php echo e($order->status); ?>">

                                    <button type="submit" class="btn btn-primary approve-btn">
                                        <?php if($order->status == 0): ?>
                                            Approve?
                                        <?php elseif($order->status == 1): ?>
                                            Send Location?
                                        <?php else: ?>
                                            Finish?
                                        <?php endif; ?>
                                    </button>
                                </form>

                            <form class="m-2" id="cancelConfirmation<?php echo e($order->id); ?>" action="<?php echo e(route('order.cancel_status', ['orderId' => $order->id])); ?>" method="POST">
                                <?php echo csrf_field(); ?>
                                <?php echo method_field('POST'); ?>

                                <?php if($order->status == 1 /*|| $order->status == 2*/): ?>
                                <button type="button" class="btn btn-danger approve-btn" onclick="cancelConfirmation('<?php echo e($order->name); ?>', '<?php echo e($order->id); ?>')">
                                    Cancel
                                </button>

                                <?php endif; ?>

                            </form>
                            <?php elseif($order->status == 3): ?>
                                <button disabled class="btn btn-secondary">Waiting approval customer</button>
                            <?php else: ?>
                                Delivered
                            <?php endif; ?>

                        </td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    function showConfirmation(orderName, id) {
        Swal.fire({
            title: 'Are you sure?',
            text: `Do you want to approve the order for ${orderName}?`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, approve it!'
        }).then((result) => {
            if (result.isConfirmed) {
                document.getElementById('confirmForm' + id).submit();
            }
        });
    }

    function cancelConfirmation(orderName, id) {
        Swal.fire({
            title: 'Are you sure?',
            text: `Do you want to cancel the order for ${orderName}?`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, cancel it!'
        }).then((result) => {
            if (result.isConfirmed) {
                document.getElementById('cancelConfirmation' + id).submit();
            }
        });
    }
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.masterdash', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/csoy8512/laravel/resources/views/user/order.blade.php ENDPATH**/ ?>