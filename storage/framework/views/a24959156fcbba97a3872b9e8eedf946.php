<?php $__env->startSection('title'); ?>
    Receive Order
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="card">
    <div class="card-header">
        <div class="card-title">
            Order
        </div>
    </div>
    <div class="row card-body">
        <table id="kt_datatable_example_1" class="table table-row-bordered gy-5">
            <thead>
                <tr class="fw-bold fs-6 text-muted">
                    <th>Name</th>
                    <th>Address</th>
                    <th>Trash Volume (Kg)</th>
                    <th>Pick up Date</th>
                    <th>Salary</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Ikhsan Furqon</td>
                    <td>Mustika Jaya</td>
                    <td>10</td>
                    <td>1/05/24</td>
                    <td>Rp. 124.000</td>
                    <td>
                        <button id="approveBtn" class="btn btn-primary" onclick="showConfirmation('Ikhsan Furqon')">Approve</button>
                    </td>
                </tr>
                <tr>
                    <td>Jamilah</td>
                    <td>Setu</td>
                    <td>15</td>
                    <td>3/05/24</td>
                    <td>Rp. 150.000</td>
                    <td>
                        <button id="approveBtn" class="btn btn-primary" onclick="showConfirmation('Ikhsan Furqon')">Approve</button>
                    </td>
                </tr>
                <!-- Add more table rows for other orders -->
            </tbody>
        </table>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    function showConfirmation(orderName) {
        Swal.fire({
            title: 'Are you sure?',
            text: `Do you want to approve the order for ${orderName}?`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, approve it!'
        }).then((result) => {
            if (result.isConfirmed) {
                Swal.fire(
                    'Approved!',
                    `The order for ${orderName} has been approved.`,
                    'success'
                );

                // Update the content of the cell to indicate the data has been taken
                var approveCell = document.getElementById('approveBtn').parentNode;
                if (approveCell) {
                    approveCell.innerHTML = 'Customer Taken';
                }
            }
        });
    }
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.masterdash', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Owner\Downloads\app-main\resources\views\user\order.blade.php ENDPATH**/ ?>