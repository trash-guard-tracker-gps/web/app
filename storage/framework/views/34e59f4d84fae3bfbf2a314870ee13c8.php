<?php $__env->startSection('title'); ?>
    GPS
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="card">
    <div class="row card-body">
        <div id="map" class="h-450px shadow rounded border border-light border-5 flex-grow-1"></div>
    </div>
</div>

<!-- Description details for routes -->
<div class="card mt-4">
    <div class="card-body">
        <h5>Route Details</h5><br>
        <ul>
            <li><strong>Driver: Yuli Yanto</strong><br>Car Number: B 9013 FOQ<br>Route: Rumah (Sumur Batu) to Rumah (Sumur Batu)</li><br>
            <li><strong>Driver: Yaman</strong><br>Car Number: B 9145 FOQ<br>Route: Parkir mobil (Kp. Cisaat Desa Kerta Rahayu JL. Raya Setu Serang) to Parkir mobil (Kp. Cisaat Desa Kerta Rahayu Jl. Raya Setu Serang)</li><br>
            <li><strong>Driver: Sabrih</strong><br>Car Number: B 9004 FOQ<br>Route: JL.Hasanudin to Setu TPA</li>
        </ul>
    </div>
</div>

<script src="https://unpkg.com/leaflet@1.9.4/dist/leaflet.js"
integrity="sha256-20nQCchB9co0qIjJZRGuk2/Z9VM+kNiyxNV1lvTlZBo="
crossorigin=""></script>
<script>
    const tengah = [-6.462562, 106.914157];
    var map = L.map('map').setView(tengah, 10);
    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributor',
        //other attributes.
    }).addTo(map);

    // Define the truck markers
    const truck1Marker = L.marker([-6.462562, 106.914157]).addTo(map);
    truck1Marker.bindPopup("<b>Truck: B 9013 FOQ</b><br>Driver: Yuli Yanto");

    const truck2Marker = L.marker([-6.454066, 107.021164]).addTo(map);
    truck2Marker.bindPopup("<b>Truck: B 9145 FOQ</b><br>Driver: Yaman");

    const truck3Marker = L.marker([-6.472321, 106.843008]).addTo(map);
    truck3Marker.bindPopup("<b>Truck: B 9004 FOQ</b><br>Driver: Sabrih");

    // Define the route coordinates for truck 1
    const routeCoordinates1 = [
        [-6.462562, 106.914157], // Rumah (Sumur Batu)
        [-6.477192, 106.921728], // Jln Pangkalan II Burangkeng
        [-6.482407, 106.937733], // Jln Cinyongsong
        [-6.469935, 106.967863], // Jln Raya Setu
        [-6.356332, 107.005429], // Jln Inpeksi Kalimalang
        [-6.353055, 107.016163], // Jln Mustika Jaya
        [-6.312774, 107.031993], // Jln Boulevard Grand Wisata
        [-6.294986, 107.115866], // Jln Raya Teuku Umar
        [-6.310873, 107.150291], // SPBU 34.1750.07(Jl Fatahilah No.89 Kec Cikarang Barat)
        [-6.315497, 107.158243], // Jln RE Martadinata
        [-6.314050, 107.163454], // Jln Jend Urip Sumoharjo
        [-6.330799, 107.173563], // Jln Raya Rengasbandung
        [-6.340105, 107.178482], // Jln Raya Bojong Sari
        [-6.361514, 107.194791], // Perumahan Kedung Waringin
        [-6.373486, 107.189332], // Jln Kedung Waringin
        [-6.384089, 107.173908], // Jln Raya Rengasbandung
        [-6.400578, 107.158149], // Jln Urip Sumoharjo
        [-6.359607, 107.063919], // Jln Inpeksi Kalimalang
        [-6.353055, 107.016163], // Jln Mustika Jaya
        [-6.469935, 106.967863], // Jln Raya Setu
        [-6.356332, 107.005429], // TPA Burangkeng
        [-6.462562, 106.914157], // Rumah
    ];

    // Define the route polyline options for truck 1
    const polylineOptions1 = {
        color: 'red',
        weight: 3,
        opacity: 0.7
    };

    // Create the route polyline for truck 1 and add it to the map
    const routePolyline1 = L.polyline(routeCoordinates1, polylineOptions1).addTo(map);

    // Define the route coordinates for truck 2
    const routeCoordinates2 = [
        [-6.454066, 107.021164], // Parkir mobil (Kp. Cisaat Desa Kerta Rahayu JL. Raya Setu Serang)
        [-6.462562, 106.914157], // JL. Raya setu
        [-6.356332, 107.005429], // JL. Inpeksi Kalimalang
        [-6.299387, 106.974495], // JL. Akses Tol Cibitung
        [-6.237213, 107.038199], // JL. Raya Pantura
        [-6.231046, 107.030160], // SPBU 34-17536 (Kalijaya)
        [-6.270682, 107.019904], // JL. Raya Lemah Abang
        [-6.277778, 107.186083], // Kawasan Industri Jababeka
        [-6.277917, 107.269546], // JL. Raya Tegal Danas
        [-6.261674, 107.303450], // JL. Deltamas Boulvard
        [-6.259226, 107.297842], // JL. Cicau
        [-6.261674, 107.303450], // JL. Deltamas Boulvard
        [-6.277917, 107.269546], // JL. Raya Tegal Danas
        [-6.277778, 107.186083], // Kawasan Industri Jababeka
        [-6.270682, 107.019904], // JL. Raya Lemah Abang
        [-6.231046, 107.030160], // SPBU 34-17536 (Kalijaya)
        [-6.237213, 107.038199], // JL. Raya Pantura
        [-6.299387, 106.974495], // JL. Akses Tol Cibitung
        [-6.356332, 107.005429], // JL. Inpeksi Kalimalang
        [-6.462562, 106.914157], // JL. Raya setu
        [-6.454066, 107.021164], // Parkir mobil (Kp. Cisaat Desa Kerta Rahayu Jl. Raya Setu Serang)
    ];

    // Define the route polyline options for truck 2
    const polylineOptions2 = {
        color: 'blue',
        weight: 3,
        opacity: 0.7
    };

    const routePolyline2 = L.polyline(routeCoordinates2, polylineOptions2).addTo(map);

    // Define the route coordinates for truck 3
    const routeCoordinates3 = [
        [-6.472321, 106.843008], // JL.Hasanudin
        [-6.469628, 106.855088], // JL.Imam Bonjol
        [-6.473306, 106.869191], // JL.Fatahillah
        [-6.472322, 106.884196], // JL.Raya Lemah Abang
        [-6.474056, 106.897768], // Bapelkes
        [-6.479002, 106.907557], // Desa Simpangan
        [-6.472322, 106.884196], // JL.Raya Lemah Abang
        [-6.394569, 106.910421], // JL.Urip Sumiharjo
        [-6.469628, 106.855088], // JL.Imam Bonjol
        [-6.478504, 106.916871], // JL.Raya Cibuntu
        [-6.475005, 106.944480], // Setu TPA
    ];

    // Define the route polyline options for truck 3
    const polylineOptions3 = {
        color: 'green',
        weight: 3,
        opacity: 0.7
    };

    // Create the route polyline for truck 3 and add it to the map
    const routePolyline3 = L.polyline(routeCoordinates3, polylineOptions3).addTo(map);

    function calculateDelay(routeCoordinates, speed) {
        const totalPoints = routeCoordinates.length;
        const totalDuration = 5 * 60 * 1000; // 5 minutes in milliseconds
        const delay = totalDuration / (totalPoints - 1);
        return delay / speed; // Adjust speed
    }

    // Move markers along the route
    function moveMarker(marker, routeCoordinates, speed) {
        let index = 0;
        const delay = calculateDelay(routeCoordinates, speed);

        function move() {
            marker.setLatLng(routeCoordinates[index]);

            index++;
            if (index < routeCoordinates.length) {
                setTimeout(move, delay);
            }
        }

        move();
    }

    // Start moving markers
    moveMarker(truck1Marker, routeCoordinates1, 1); // Adjust speed here
    moveMarker(truck2Marker, routeCoordinates2, 1); // Adjust speed here
    moveMarker(truck3Marker, routeCoordinates3, 1); // Adjust speed here

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.masterdash', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Owner\Downloads\app-main\resources\views/user/gps.blade.php ENDPATH**/ ?>