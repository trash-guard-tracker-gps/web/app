<?php $__env->startSection('title'); ?>
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="card">
    <div class="card-header">
        <div class="card-title">
            Customer
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table id="kt_datatable_example_1" class="table table-row-bordered gy-5">
                <thead>
                    <tr class="fw-bold fs-6 text-muted">
                        <th>No</th>
                        <th>Name</th>
                        <th>Phone Number</th>
                        <th>Address</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                <?php $__currentLoopData = $customers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $customer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr data-user-id="<?php echo e($customer->id); ?>">
                    <td><?php echo e($index + 1); ?></td>
                    <td><?php echo e($customer->name); ?></td>
                    <td><?php echo e($customer->phone); ?></td>
                    <td><?php echo e($customer->address); ?></td>

                    <td>
                        <form id="deleteForm<?php echo e($customer->id); ?>" method="POST" action="<?php echo e(route('customer.delete_by_id', $customer->id)); ?>">
                            <?php echo csrf_field(); ?>
                            <button type="submit" class="btn btn-primary delete-btn" data-user-id="<?php echo e($customer->id); ?>">Delete</button>
                        </form>
                    </td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
$('.delete-btn').click(function(event) {
    event.preventDefault();
    var row = $(this).closest('tr');
    var userId = $(this).data('user-id');

    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.isConfirmed) {
            $('#deleteForm' + userId).submit();
        }
    });
});
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.masterdash', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\csow\web_app\resources\views/user/customer.blade.php ENDPATH**/ ?>