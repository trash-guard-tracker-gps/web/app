</div>
<!--end::Testimonials Section-->

</div>
<!--end::Container-->
</div>
<!--end::Post-->
</div>
<!--end::Root-->

<!--begin::Scrolltop-->
<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
    <!--begin::Svg Icon | path: icons/duotune/arrows/arr066.svg-->
    <span class="svg-icon">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
            <rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)"
                fill="black" />
            <path
                d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z"
                fill="black" />
        </svg>
    </span>
    <!--end::Svg Icon-->
</div>

<script>
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "3000",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
</script>
<!--end::Scrolltop-->
<script>
    var hostUrl = "assets/";
</script>
<!--begin::Javascript-->
<!--begin::Global Javascript Bundle(used by all pages)-->
<script src="<?php echo e(asset('plugins/global/plugins.bundle.js')); ?>"></script>
<script src="<?php echo e(asset('js/scripts.bundle.js')); ?>"></script>
<script src="<?php echo e(asset('js/custom.js')); ?>"></script>
<!--end::Global Javascript Bundle-->
<!--begin::Page Vendors Javascript(used by this page)-->
<script src="<?php echo e(asset('plugins/custom/fslightbox/fslightbox.bundle.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/custom/typedjs/typedjs.bundle.js')); ?>"></script>
<!--end::Page Vendors Javascript-->
<!--begin::Page Custom Javascript(used by this page)-->
<script src="<?php echo e(asset('js/custom/landing.js')); ?>"></script>
<script src="<?php echo e(asset('js/custom/pages/company/pricing.js')); ?>"></script>
<!--end::Page Custom Javascript-->


<!--end::Page Vendors Javascript-->
<!--begin::Page Custom Javascript(used by this page)-->
<script src="<?php echo e(asset('/js/custom/apps/customers/list/export.js')); ?>"></script>
<script src="<?php echo e(asset('/js/custom/apps/customers/list/list.js')); ?>"></script>
<script src="<?php echo e(asset('/js/custom/apps/customers/add.js')); ?>"></script>
<script src="<?php echo e(asset('/js/custom/widgets.js')); ?>"></script>
<script src="<?php echo e(asset('/js/custom/apps/chat/chat.js')); ?>"></script>
<script src="<?php echo e(asset('/js/custom/modals/create-app.js')); ?>"></script>
<script src="<?php echo e(asset('/js/custom/modals/upgrade-plan.js')); ?>"></script>
<!--end::Page Custom Javascript-->
</body>
<!--end::Body-->

</html>
<?php /**PATH C:\Users\Owner\Downloads\app-main\resources\views/partials/footer.blade.php ENDPATH**/ ?>