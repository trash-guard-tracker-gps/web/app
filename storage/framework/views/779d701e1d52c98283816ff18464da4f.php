<?php $__env->startSection('title'); ?>
    Home
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="card">
    <div class="row card-body">
        <div class="col-md-4 pt-2 pt-md-10 ps-2 ps-md-10">
            <div class="row mb-3 mb-md-5">
                <h1>WELCOME</h1>
            </div>
            <div class="row mb-3 mb-md-5">
                <h3>
                    Hello, Environmental Health Officer!!
                </h3>
            </div>
            <div class="row">
                <h3>
                    Enjoy your work. Don't forget to stay safe and enthusiastic, okay?
                </h3>
            </div>
        </div>
        <div class="col-md-8 text-center">
            <img src="<?php echo e(asset('media/auth Img.png')); ?>" class="mt-n3 img-fluid" alt="">
        </div>
    </div>
</div>

<script>
    // if ("geolocation" in navigator) {
    //     navigator.geolocation.getCurrentPosition(function(position) {
    //         console.log("Latitude: " + position.coords.latitude);
    //         console.log("Longitude: " + position.coords.longitude);

    //         fetch("<?php echo e(route('user.update_lat_long')); ?>", {
    //             method: 'POST',
    //             headers: {
    //                 'Content-Type': 'application/json',
    //                 'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
    //             },
    //             body: JSON.stringify({
    //                 latitude: position.coords.latitude,
    //                 longitude: position.coords.longitude
    //             })
    //         })
    //         .then(response => response.json())
    //         .then(data => console.log(data))
    //         .catch(error => console.error('Error:', error));
    //     });
    // } else {
    //     console.log("Geolocation is not supported by this browser.");
    // }
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.masterdash', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\csow\web_app\resources\views/user/dashboard.blade.php ENDPATH**/ ?>