<!DOCTYPE html>
<html lang="en">
<head>
  <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title><?php echo $__env->yieldContent('title'); ?> CSOW - Login</title>
  <link rel="shortcut icon" href="<?php echo e(asset('media/Logo.png')); ?>" />
  
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />

  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo e(asset('plugins/global/plugins.bundle.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(asset('css/style.bundle.css')); ?>">

  <!-- CSS Libraries -->
  <link rel="stylesheet" href="<?php echo e(asset('modules/jqvmap/dist/jqvmap.min.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(asset('modules/weather-icon/css/weather-icons.min.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(asset('modules/weather-icon/css/weather-icons-wind.min.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(asset('modules/summernote/summernote-bs4.css')); ?>">

  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo e(asset('css/style.bundle.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(asset('css/components.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(asset('css/custom.css')); ?>">
<!-- Start GA -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-94034622-3');
</script>
<!-- /END GA --></head>
<body id="kt_body">

    <?php echo $__env->yieldContent('content'); ?>
  </div>

<!-- General JS Scripts -->
<script src="<?php echo e(asset('plugins/global/plugins.bundle.js')); ?>"></script>
<script src="<?php echo e(asset('js/scripts.bundle.js')); ?>"></script>

<!-- JS Libraies -->
<script src="<?php echo e(asset('modules/jquery-pwstrength/jquery.pwstrength.min.js')); ?>"></script>
<script src="<?php echo e(asset('modules/jquery-selectric/jquery.selectric.min.js')); ?>"></script>

<!-- Page Specific JS File -->
<script src="<?php echo e(asset('js/page/auth-register.js')); ?>"></script>

<!-- Template JS File -->
<script src="<?php echo e(asset('js/scripts.js')); ?>"></script>
<script src="<?php echo e(asset('js/custom/authentication/sign-up/general.js')); ?>"></script>
<script src="<?php echo e(asset('js/custom/authentication/sign-in/general.js')); ?>"></script>
</body>
</html>
<?php /**PATH C:\Users\Owner\Downloads\app-main\resources\views\layout\masterauth.blade.php ENDPATH**/ ?>