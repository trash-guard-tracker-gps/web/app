<?php echo $__env->make('partials.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div id="kt_content_container" class="m-n2">
                          <?php echo $__env->yieldContent('content'); ?>
                      </div>
      <?php echo $__env->make('partials.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /**PATH /home/csoy8512/laravel/resources/views/layout/masterdash.blade.php ENDPATH**/ ?>