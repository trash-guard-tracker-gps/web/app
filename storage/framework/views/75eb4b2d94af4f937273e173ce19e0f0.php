<?php $__env->startSection('title', 'GPS'); ?>

<?php $__env->startSection('content'); ?>
<div class="card">
    <div class="row card-body">
        <div id="map" class="h-450px shadow rounded border border-light border-5 flex-grow-1"></div>
    </div>
</div>

<!-- Description details for routes -->
<div class="card mt-4">
    <div class="card-body">
        <h5>Route Details</h5><br>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Driver</th>
                        <th>Type</th>
                        <th>Latitude</th>
                        <th>Longitude</th>
                        <th>Customer Phone Number</th>
                        <th>Pickup Address</th>
                        <th>Pickup Additional Information</th>
                        <th>Pickup Latitude</th>
                        <th>Pickup Longitude</th>
                    </tr>
                </thead>
                <tbody id="userTableBody"></tbody>
            </table>
        </div>
    </div>
</div>

<link rel="stylesheet" href="https://unpkg.com/leaflet@1.2.0/dist/leaflet.css" />
<link rel="stylesheet" href="https://unpkg.com/leaflet-routing-machine@latest/dist/leaflet-routing-machine.css" />
<script src="https://unpkg.com/leaflet@1.2.0/dist/leaflet.js"></script>
<script src="https://unpkg.com/leaflet-routing-machine@latest/dist/leaflet-routing-machine.js"></script>

<script src="https://cdn.socket.io/4.5.0/socket.io.min.js" integrity="sha384-7EyYLQZgWBi67fBtVxw60/OWl1kjsfrPFcaU0pp0nAh+i8FD068QogUvg85Ewy1k" crossorigin="anonymous"></script>

<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

<script>
    const tengah = [-6.462562, 106.914157];
    var map = L.map('map').setView(tengah, 10);
    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributor',
    }).addTo(map);

    function updateMarkers() {

        fetch("<?php echo e(route('gps.get')); ?>")
            .then(response => response.json())
            .then(data => {
                map.eachLayer(function(layer) {
                    if (layer instanceof L.Marker || layer instanceof L.Routing.Control) {
                        map.removeLayer(layer);
                    }
                });

                $('#userTableBody').empty();

                console.log("data", data)

                data.result.forEach(function(location, index) {
                var markerIcon = L.icon({ iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-red.png'  });

                    // var userMarker = L.marker([location.user_lat, location.user_long]).addTo(map);
                    // var custMarker = L.marker([location.cust_lat, location.cust_long]).addTo(map);

                    var userLocation = L.latLng(location.user_lat, location.user_long);
                    var custLocation = L.latLng(location.cust_lat, location.cust_long);

                    var control = L.Routing.control({
                        waypoints: [userLocation, custLocation],
                        lineOptions: {
                            styles: [{color: location.id == <?php echo e($user_id); ?> ? '#FF0000' : '#0000FF'}]
                        },
                    }).addTo(map);

                    const routingControlContainer = control.getContainer();
                    routingControlContainer.style.width = '40%';


                    $('#userTableBody').append(`
                        <tr>
                            <td>${index + 1}</td>
                            <td>You</td>
                            <td>${location.cust_phone_number ? "Order" : "Daily"}</td>
                            <td>${location.user_lat}</td>
                            <td>${location.user_long}</td>
                            <td>${location.cust_phone_number || '-'}</td>
                            <td>${location.cust_address}</td>
                            <td>${location.add_information || '-'}</td>
                            <td>${location.cust_lat}</td>
                            <td>${location.cust_long}</td>
                        </tr>
                    `);
                });
            })
            .catch(error => console.error('Error fetching user data:', error));
    }

    function sendLocationToServer() {
        navigator.geolocation.getCurrentPosition(function(position) {
            fetch("<?php echo e(route('user.update_lat_long')); ?>", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
                },
                body: JSON.stringify({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude
                })
            })
            .then(response => response.json())
            .then(data => console.log(data))
            .catch(error => console.error('Error sending location to server:', error));
        });
    }

    updateMarkers();

    setInterval(updateMarkers, 3000);

    sendLocationToServer();

    setInterval(sendLocationToServer, 3000);
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.masterdash', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/escurtcheon/Documents/Code/trash_guard_gps_tracker/web/update-angga-1/app-main/resources/views/user/gps.blade.php ENDPATH**/ ?>