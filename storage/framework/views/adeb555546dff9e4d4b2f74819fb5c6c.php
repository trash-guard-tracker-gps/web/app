<?php $__env->startSection('title'); ?>
    Register
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<style>
    /* CSS for responsiveness */
@media (max-width: 1199px) {
    .w-xl-800px {
        width: 100%;
    }
}

@media (max-width: 767px) {
    .h-300px {
        height: auto;
        max-width: 100%;
    }
}

</style>
    <div class="d-flex flex-column flex-root">
        <!--begin::Authentication - Register -->
        <div class="d-flex flex-column flex-lg-row flex-column-fluid">
            <!--begin::Aside-->
            <div class="d-flex flex-column flex-lg-row-auto w-xl-800px positon-xl-relative">
                <!--begin::Wrapper-->
                <div class="d-flex flex-column position-xl-fixed top-0 bottom-0 w-xl-800px scroll-y">
                    <!--begin::Content-->
                    <div class="d-flex flex-row-fluid flex-column text-center p-10 pt-lg-20">
                        <!--begin::Logo-->
                        <a href="/" class="m-auto">
                            <img alt="Logo" src="<?php echo e(asset('media/image 2.png')); ?>" class="h-300px" />
                        </a>
                        <!--end::Logo-->

                    </div>
                    <!--end::Content-->
                </div>
                <!--end::Wrapper-->
            </div>
            <!--end::Aside-->
            <!--begin::Body-->
            <div class="d-flex bg-secondary flex-column flex-lg-row-fluid py-10">
                <!--begin::Content-->
                <div class="d-flex flex-center flex-column flex-column-fluid">
                    <!--begin::Wrapper-->
                    <div class="w-lg-500px p-10 p-lg-15 mx-auto">
                        <!--begin::Form-->
                        <form class="form w-100" novalidate="novalidate"
                            action="<?php echo e(route('register')); ?>" method="POST">
                            <?php echo csrf_field(); ?>
                            <!--begin::Heading-->
                            <div class="text-center mb-10">
                                <!--begin::Logo-->
                                <a href="/" class="py-9 mb-5">
                                    <img alt="Logo" src="<?php echo e(asset('media/Logo.png')); ?>" class="h-75px" />
                                </a>
                                <!--end::Logo-->
                                <!--begin::Title-->
                                <h2 class="text-dark mb-3"><span style="color: #86DA36">Cikarang Smart</span> <span style="color: #3A9106">Organization Waste</span></h2>
                                <h2 class="text-dark mb-3">Register to CSOW Fleet</h2>
                                <!--end::Title-->
                                <!--begin::Link-->
                                <div class="text-black-400 fw-bold fs-4">Already have an account?
                                    <a href="<?php echo e(route('login')); ?>" class="link-primary fw-bolder">Sign In</a>
                                </div>
                                <!--end::Link-->
                            </div>

                            <?php if(Session::has('error')): ?>
                            <div class="alert alert-danger" role="alert">
                                <?php echo e(Session::get('error')); ?>

                            </div>
                            <?php endif; ?>
                            <!--end::Heading-->
                            <!--begin::Input group-->
                            <div class="fv-row mb-10">
                                <!--begin::Label-->
                                <label class="form-label fs-6 fw-bolder text-dark">Name</label>
                                <!--end::Label-->
                                <!--begin::Input-->
                                <input class="form-control form-control-lg form-control-solid" type="text" name="name"
                                    autocomplete="off" placeholder="name" />
                                <!--end::Input-->
                            </div>
                            <!--end::Input group-->
                            <!--begin::Input group-->
                            <div class="fv-row mb-10">
                                <!--begin::Label-->
                                <label class="form-label fs-6 fw-bolder text-dark">Email</label>
                                <!--end::Label-->
                                <!--begin::Input-->
                                <input class="form-control form-control-lg form-control-solid" type="email" name="email"
                                    autocomplete="off" placeholder="name@example.com" />
                                <!--end::Input-->
                            </div>
                            <!--end::Input group-->
                            <!--begin::Input group-->
                            <div class="fv-row mb-10">
                                <!--begin::Wrapper-->
                                <div class="d-flex flex-stack mb-2">
                                    <!--begin::Label-->
                                    <label class="form-label fw-bolder text-dark fs-6 mb-0">Password</label>
                                    <!--end::Label-->
                                    <!--begin::Link-->
                                    
                                    <!--end::Link-->
                                </div>
                                <!--end::Wrapper-->
                                <!--begin::Input-->
                                <input class="form-control form-control-lg form-control-solid" type="password"
                                    name="password" autocomplete="off" />
                                <!--end::Input-->
                            </div>
                            <!--end::Input group-->
                            <!--begin::Actions-->
                            <div class="text-center">
                                <!--begin::Submit button-->
                                <button type="submit" id="kt_sign_in_submit" class="btn btn-lg btn-primary w-50 mb-5">
                                    <span class="indicator-label">Sign Up</span>
                                    <span class="indicator-progress">Please wait...
                                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                </button>
                                <!--end::Submit button-->
                            </div>
                            <!--end::Actions-->
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Wrapper-->
                </div>
                <!--end::Content-->
                <!--begin::Footer-->
                <div class="d-flex flex-center flex-wrap fs-6 p-5 pb-0">
                    <!--begin::Links-->
                    <div class="d-flex flex-center fw-bold fs-6 p-1 bg-dark rounded">
                        <a href="/" class="text-muted text-hover-primary px-2">Back</a>
                    </div>
                    <!--end::Links-->
                </div>
                <!--end::Footer-->
            </div>
            <!--end::Body-->
        </div>
        <!--end::Authentication - Register-->
    </div>

    <!-- Success Popup -->
    <div class="modal fade" id="successPopup" tabindex="-1" aria-labelledby="successPopupLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <div class="my-3">
                        <i class="fas fa-check-circle fa-5x text-success"></i>
                    </div>
                    <h3 class="modal-title" id="successPopupLabel">Registration Successful!</h3>
                    <p>You have successfully registered.</p>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        // Show success popup after successful registration
        $(document).ready(function() {
            <?php if(Session::has('success')): ?>
                $('#successPopup').modal('show');
            <?php endif; ?>
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.masterauth', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/escurtcheon/Documents/Code/trash_guard_gps_tracker/web/update-angga-1/app-main/resources/views/auth/register.blade.php ENDPATH**/ ?>