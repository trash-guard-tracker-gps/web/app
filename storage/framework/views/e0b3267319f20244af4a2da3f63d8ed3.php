<?php $__env->startSection('title'); ?>
    Customer
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="card">
    <div class="card-header">
        <div class="card-title">
            Customer
        </div>
    </div>
    <div class="row card-body">
        <table id="kt_datatable_example_1" class="table table-row-bordered gy-5">
            <thead>
                <tr class="fw-bold fs-6 text-muted">
                    <th>Name</th>
                    <th>Address</th>
                    <th>Phone</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Jamilah</td>
                    <td>Setu</td>
                    <td>0813 9176 2223</td>
                    <td>
                        <button class="btn btn-primary delete-btn">Delete</button>
                    </td>
                </tr>
                <tr>
                    <td>Ikhsan Furqon</td>
                    <td>Mustika Jaya</td>
                    <td>0812 4651 1223</td>
                    <td>
                        <button class="btn btn-primary delete-btn">Delete</button>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    $(document).ready(function() {
        // Add click event listener to all delete buttons
        $('.delete-btn').click(function() {
            var row = $(this).closest('tr');
            // Show confirmation popup
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    // Remove the row if user confirms
                    row.remove();
                    Swal.fire(
                        'Deleted!',
                        'Your data has been deleted.',
                        'success'
                    );
                }
            });
        });
    });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.masterdash', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Owner\Downloads\app-main\resources\views\user\customer.blade.php ENDPATH**/ ?>