<?php $__env->startSection('title'); ?>
    Sensor Volume
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="card">
    <div class="card-header">
        <div class="card-title">
            Sensor
        </div>
    </div>
    <div class="row card-body">
        <!-- tampilan header -->
        <div class="container" style="text-align: center; margin-top: 50px;">
            <h1>Waste Monitoring</h1>
        </div>
        <!-- tampilan sensor -->
        <div class="container" style="margin-top:30px;">
            <div class="row" style="text-align:center;">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header" style="text-align:center; background-color: white; color: blue;">
                            <h4>Trash Level</h4>
                        </div>
                        <div class="card-body">
                            <div style="font-size: 50px; font-weight:bold;">
                                <span id="sensor"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header" style="text-align:center; background-color: white; color: green;">
                            <h4>Trash Status</h4>
                        </div>
                        <div class="card-body">
                            <div style="font-size: 50px; font-weight:bold;">
                                <span id="status"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- tampilan data table -->
        <div class="container" style="text-align: center; margin-top: 30px;">
            <h3>History Table</h3>
        </div>
        <div>
            <table class="table" style="width:90%; text-align:center; margin-left:5%; margin-right:5%; margin-top:30px;">
                <thead>
                    <tr>
                        <th style="width:10%" scope="col">No</th>
                        <th style="width:20%" scope="col">Level</th>
                        <th style="width:20%" scope="col">Status</th>
                        <th style="width:40%" scope="col">Address</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $sensors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $sensor): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <th scope="row"><?php echo e($index + 1); ?></th>
                        <td id="sensor_table"><?php echo e($sensor->sensor); ?></td>
                        <td id="status_table"><?php echo e($sensor->status); ?></td>
                        <td id="alamat"><?php echo e($sensor->alamat); ?></td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
        <div style="margin-top: 70px; margin-left:5%;">
            <button type="button" class="btn btn-primary">Back</button>
        </div>
        <!-- jQuery and AJAX script -->
        <script type="text/javascript" src="<?php echo e(asset('jquery/jquery.min.js')); ?>"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                setInterval(function(){
                    $.ajax({
                        url: "<?php echo e(route('sensor.level_sensor')); ?>",
                        method: 'GET',
                        success: function(data) {
                            $("#sensor").text(data.sensor);
                            $("#sensor_table").text(data.sensor);
                        }
                    });php

                    $.ajax({
                        url: "<?php echo e(route('sensor.status_sensor')); ?>",
                        method: 'GET',
                        success: function(data) {
                            $("#status").text(data.status);
                            $("#status_table").text(data.status);
                        }
                    });

                    $.ajax({
                        url: "<?php echo e(route('sensor.alamat_sensor')); ?>",
                        method: 'GET',
                        success: function(data) {
                            $("#alamat").text(data.alamat);
                        }
                    });
                }, 1000);
            });
        </script>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.masterdash', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/escurtcheon/Documents/Code/trash_guard_gps_tracker/web/update-angga-1/app-main/resources/views/user/sensor.blade.php ENDPATH**/ ?>