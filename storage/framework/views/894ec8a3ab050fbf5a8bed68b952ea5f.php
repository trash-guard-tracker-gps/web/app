<?php echo $__env->make('partials.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div id="kt_content_container" class="mx-10 my-20 mx-xl-10 my-xl-10">
                          <?php echo $__env->yieldContent('content'); ?>
                      </div>
      <?php echo $__env->make('partials.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /**PATH C:\Users\Owner\Downloads\app-main\resources\views\layout\master.blade.php ENDPATH**/ ?>