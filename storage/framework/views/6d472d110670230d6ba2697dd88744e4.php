<?php $__env->startComponent('mail::message'); ?>
# Forgot Password

You are receiving this email because you requested a password reset.

Your OTP (One-Time Password) is: **<?php echo e($otp); ?>**

This OTP will expire after a short period of time.

If you did not request a password reset, no further action is required.

Thanks,<br>
<?php echo e(config('app.name')); ?>

<?php echo $__env->renderComponent(); ?>
<?php /**PATH /home/csoy8512/laravel/resources/views/emails/forgot_password_mobile.blade.php ENDPATH**/ ?>