<?php $__env->startSection('title'); ?>
    Login
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="d-flex flex-column flex-root">
        <!--begin::Authentication - Sign-in -->
        <div class="d-flex flex-column flex-lg-row flex-column-fluid">
            <!--begin::Aside-->
            <div class="d-flex flex-column flex-lg-row-auto w-xl-800px positon-xl-relative">
                <!--begin::Wrapper-->
                <div class="d-flex flex-column position-xl-fixed top-0 bottom-0 w-xl-800px scroll-y">
                    <!--begin::Content-->
                    <div class="d-flex flex-row-fluid flex-column text-center p-10 pt-lg-20">
                        <!--begin::Logo-->
                        <a href="/" class="m-auto">
                            <img alt="Logo" src="<?php echo e(asset('media/image 2.png')); ?>" class="h-300px" />
                        </a>
                        <!--end::Logo-->

                    </div>
                    <!--end::Content-->
                </div>
                <!--end::Wrapper-->
            </div>
            <!--end::Aside-->
            <!--begin::Body-->
            <div class="d-flex bg-secondary flex-column flex-lg-row-fluid py-10">
                <!--begin::Content-->
                <div class="d-flex flex-center flex-column flex-column-fluid">
                    <!--begin::Wrapper-->
                    <div class="w-lg-500px p-10 p-lg-15 mx-auto">
                        <?php if(Session::has('error')): ?>
                            <script>
                                // Display SweetAlert with the error message
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Error',
                                    text: '<?php echo e(Session::get('error')); ?>',
                                    confirmButtonText: 'OK'
                                });
                            </script>
                        <?php endif; ?>

                        <!--begin::Heading-->
                        <div class="text-center mb-10">
                            <!--begin::Logo-->
                            <a href="/" class="py-9 mb-5">
                                <img alt="Logo" src="<?php echo e(asset('media/Logo.png')); ?>" class="h-75px" />
                            </a>
                            <!--end::Logo-->
                            <!--begin::Title-->
                            <h2 class="text-dark mb-3"><span style="color: #86DA36">Cikarang Smart</span> <span style="color: #3A9106">Organization Waste</span></h2>
                            <!--end::Title-->
                            <!--begin::Link-->
                            <div class="text-black-400 fw-bold fs-4">New Here?
                                <a href="<?php echo e(route('register')); ?>" class="link-primary fw-bolder">Create an Account</a>
                            </div>
                            <!--end::Link-->
                        </div>
                        <!--begin::Heading-->
                        <!--begin::Form-->
                        <form class="form" id="kt_sign_in_form" action="<?php echo e(route('login')); ?>" method="POST">
                            <?php echo csrf_field(); ?>
                            <!--begin::Input group-->
                            <div class="fv-row mb-10">
                                <!--begin::Label-->
                                <label class="form-label fs-6 fw-bolder text-dark">Email</label>
                                <!--end::Label-->
                                <!--begin::Input-->
                                <input class="form-control form-control-lg form-control-solid" type="email" name="email"
                                    autocomplete="off" placeholder="name@example.com" />
                                <!--end::Input-->
                            </div>
                            <!--end::Input group-->
                            <!--begin::Input group-->
                            <div class="fv-row mb-10">
                                <!--begin::Wrapper-->
                                <div class="d-flex flex-stack mb-2">
                                    <!--begin::Label-->
                                    <label class="form-label fw-bolder text-dark fs-6 mb-0">Password</label>
                                    <!--end::Label-->
                                    <!--begin::Link-->
                                    
                                    <!--end::Link-->
                                </div>
                                <!--end::Wrapper-->
                                <!--begin::Input-->
                                <input class="form-control form-control-lg form-control-solid" type="password"
                                    name="password" autocomplete="off" />
                                <!--end::Input-->
                            </div>
                            <!--end::Input group-->
                            <!--begin::Actions-->
                            <div class="text-center">
                                <!--begin::Submit button-->
                                <input type="submit" value="Log In" style="background: #608A51;" class="btn btn-lg text-white w-50 mb-5">
                                <span class="indicator-progress">Please wait...
                                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                </input>
                                <!--end::Submit button-->
                            </div>
                            <!--end::Actions-->
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Wrapper-->
                </div>
                <!--end::Content-->
                <!--begin::Footer-->
                <div class="d-flex flex-center flex-wrap fs-6 p-5 pb-0">
                    <!--begin::Links-->
                    <div class="d-flex flex-center fw-bold fs-6 p-1 bg-dark rounded">
                        <a href="/" class="text-muted text-hover-primary px-2">Back</a>
                    </div>
                    <!--end::Links-->
                </div>
                <!--end::Footer-->
            </div>
            <!--end::Body-->
        </div>
        <!--end::Authentication - Sign-in-->
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.masterauth', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Owner\Downloads\app-main\resources\views/auth/login.blade.php ENDPATH**/ ?>