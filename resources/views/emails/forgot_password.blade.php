<x-mail::message>
    <h1>Welcome to CSWO!</h1>

    <p>You have successfully registered with CSWO. We're thrilled to have you on board!</p>

    <x-mail::button :url="'https://example.com'">
        Explore CSWO
    </x-mail::button>

    <p>Thanks for joining us!<br>
    {{ config('app.name') }}</p>
</x-mail::message>
