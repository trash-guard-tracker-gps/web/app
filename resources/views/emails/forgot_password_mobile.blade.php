@component('mail::message')
# Forgot Password

You are receiving this email because you requested a password reset.

Your OTP (One-Time Password) is: **{{ $otp }}**

This OTP will expire after a short period of time.

If you did not request a password reset, no further action is required.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
