@extends('layout.masterdash')
@section('title')
    Sensor Volume
@endsection
@section('scripts')
@endsection
@section('content')
<div class="card">
    <div class="card-header">
        <div class="card-title">
            Sensor
        </div>
    </div>
    <div class="row card-body">
        <!-- tampilan header -->
        <div class="container" style="text-align: center; margin-top: 50px;">
            <h1>Monitoring Sampah</h1>
        </div>
            <!-- panggil jquery -->
        <script type="text/javascript" src= "{{ ('jquery/jquery.min.js') }}"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                setInterval(function(){
                    $("#level").load("{{ url('levelsensor') }}");
                    $("#status").load("{{ url('statussensor') }}");
                    $("#alamat").load("{{ url('alamatsensor') }}");
                    // Ambil nilai dari ID "level" dan "status"
                    var level = $("#level").text();
                    var status = $("#status").text();

                    // Masukkan nilai ke dalam sel yang sesuai di dalam tabel
                    $("#level_table").text(level);
                    $("#status_table").text(status);
                }, 1000);
            });
        </script>
        </script>
        <!-- tampilan sensor -->
        <div class="container" style="margin-top:30px;">
            <div class="row" style="text-align:center;">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header" style = "text-align:center; background-color: white; color: blue;">
                            <h4>Level Sampah</h4>
                        </div>
                        <div class="card-body">
                            <div style ="font-size: 50px; font-weight:bold;">
                                <span id="level">0</span>
                            </div>
                        </div>
                </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header" style = "text-align:center; background-color: white; color: green;">
                            <h4>Status Sampah</h4>
                        </div>
                        <div class="card-body">
                            <div style ="font-size: 50px; font-weight:bold">
                                <span id="status">sedikit</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- tampilan data table -->
        <div class="container" style ="text-align: center; margin-top: 30px;">
        <h3>History Table</h3>
        </div>
        <div>
            <table class="table" style = "width:90%; text-align:center; margin-left:5%; margin-right:5%; margin-top:30px;">
                <thead>
                    <tr>
                    <th style = "width:10%" scope="col">No</th>
                    <th style = "width:20%" scope="col">Level</th>
                    <th style = "width:20%" scope="col">Status</th>
                    <th style = "width:40%" scope="col">Alamat</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <th scope="row">1</th>
                    <td id = "level_table">test</td>
                    <td id = "status_table">test</td>
                    <td id="alamat">test</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!--<div style="margin-top: 70px; margin-left:5%;">-->
        <!--    <button type="button" class="btn btn-primary">Back</button>-->
        <!--</div>-->
    </div>
</div>
@endsection
