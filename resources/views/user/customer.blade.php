@extends('layout.masterdash')

@section('title')
    {{-- Title goes here --}}
@endsection

@section('content')
<div class="card">
    <div class="card-header">
        <div class="card-title">
            Customer
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table id="kt_datatable_example_1" class="table table-row-bordered gy-5">
                <thead>
                    <tr class="fw-bold fs-6 text-muted">
                        <th>No</th>
                        <th>Name</th>
                        <th>Phone Number</th>
                        <th>Address</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($customers as $index => $customer)
                <tr data-user-id="{{ $customer->id }}">
                    <td>{{ $index + 1 }}</td>
                    <td>{{ $customer->name }}</td>
                    <td>{{ $customer->phone }}</td>
                    <td>{{ $customer->address }}</td>

                    <td>
                        <form id="deleteForm{{ $customer->id }}" method="POST" action="{{ route('customer.delete_by_id', $customer->id) }}">
                            @csrf
                            <button type="submit" class="btn btn-primary delete-btn" data-user-id="{{ $customer->id }}">Delete</button>
                        </form>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
$('.delete-btn').click(function(event) {
    event.preventDefault();
    var row = $(this).closest('tr');
    var userId = $(this).data('user-id');

    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.isConfirmed) {
            $('#deleteForm' + userId).submit();
        }
    });
});
</script>
@endsection
