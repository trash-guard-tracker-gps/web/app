@extends('layout.masterdash')

@section('title', 'GPS')

@section('content')
    <div class="card">
        <div class="row card-body">
            <div id="map" class="h-450px shadow rounded border border-light border-5 flex-grow-1"></div>
        </div>
    </div>

    <!-- Description details for routes -->
    <div class="card mt-4">
        <div class="card-body">
            <h5>Route Details</h5><br>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Driver</th>
                            <th>Type</th>
                            <th>Latitude</th>
                            <th>Longitude</th>
                            <th>Customer Phone Number</th>
                            <th>Pickup Address</th>
                            <th>Pickup Additional Information</th>
                            <th>Pickup Latitude</th>
                            <th>Pickup Longitude</th>
                        </tr>
                    </thead>
                    <tbody id="userTableBody"></tbody>
                </table>
            </div>
        </div>
    </div>

    {{-- @vite('resources/js/app.js') --}}

    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.2.0/dist/leaflet.css" />
    <link rel="stylesheet" href="https://unpkg.com/leaflet-routing-machine@latest/dist/leaflet-routing-machine.css" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="https://unpkg.com/leaflet@1.2.0/dist/leaflet.js"></script>
    <script src="https://unpkg.com/leaflet-routing-machine@latest/dist/leaflet-routing-machine.js"></script>

    <script src="https://cdn.socket.io/4.5.0/socket.io.min.js"
        integrity="sha384-7EyYLQZgWBi67fBtVxw60/OWl1kjsfrPFcaU0pp0nAh+i8FD068QogUvg85Ewy1k" crossorigin="anonymous">
    </script>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <script>
        let customerLocation = {
            lat: 0,
            long: 0,
        }
        setTimeout(() => {
            window.Echo.channel('customer-location')
                .listen('CustomerLocation', (e) => {
                    console.log(e)
                    customerLocation = {
                        lat: e.lat,
                        long: e.long,
                    }
                })
        }, 200);

        // console.log("customerLocation", customerLocation);

        const tengah = [-6.462562, 106.914157];
        var map = L.map('map').setView(tengah, 10);
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributor',
        }).addTo(map);

        const sendLocationToServer = async (latitude, longitude) => {
            try {
                const response = await fetch("{{ route('location.post') }}", {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    body: JSON.stringify({
                        lat: latitude,
                        long: longitude,
                        type: "user"
                    })
                });
                const data = await response.json();
                console.log('Response:', data);
            } catch (error) {
                console.error('Error sending location to server:', error);
            }
        }

        const getCurrentLocation = () => {
            return new Promise((resolve, reject) => {
                navigator.geolocation.getCurrentPosition((position) => {
                    const latitude = position.coords.latitude;
                    const longitude = position.coords.longitude;

                    const result = {
                        lat: latitude,
                        long: longitude
                    };
                    resolve(result);
                }, (error) => {
                    reject(error);
                });
            });
        };

        const userLocation = async () => {
            try {
                const {
                    lat: userLat,
                    long: userLong
                } = await getCurrentLocation();

                await sendLocationToServer(userLat, userLong);

                return {
                    userLat,
                    userLong
                }
            } catch (error) {
                console.error('Error getting or sending location:', error);
            }
        };

        const updateMarkers = async () => {
            try {
                const {
                    userLat,
                    userLong
                } = await userLocation();

                setInterval(userLocation, 3000);

                const roleEnum = {
                    driver: 0,
                    pickUpDestination: 1
                }

                const response = await fetch("{{ route('gps.get') }}");
                const data = await response.json();

                map.eachLayer(function(layer) {
                    if (layer instanceof L.Marker || layer instanceof L.Routing.Control) {
                        map.removeLayer(layer);
                    }
                });

                $('#userTableBody').empty();

                data.result.forEach(function(location, index) {

                    L.Marker.prototype.options.icon = L.divIcon({
                        className: 'leaflet-mouse-marker',
                        iconAnchor: [20, 20],
                        iconSize: [40, 40]
                    })

                    const userIcon = L.icon({
                        iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-red.png',
                        iconAnchor: [10, 41],
                        popupAnchor: [2, -40],
                    });

                    L.marker([userLat, userLong], {
                            icon: userIcon
                        })
                        .addTo(map);

                    const custIcon = L.icon({
                        iconUrl: location.role == roleEnum.driver ?
                            'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-blue.png' :
                            'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-green.png',
                        iconAnchor: [10, 41],
                        popupAnchor: [2, -40],
                    });
                    L.marker([location.cust_lat, location.cust_long], {
                            icon: custIcon
                        })
                        .addTo(map);


                    var userLocation = L.latLng(userLat, userLong);
                    var custLocation = L.latLng(location.cust_lat, location.cust_long);

                    var control = L.Routing.control({
                        waypoints: [userLocation, custLocation],
                        lineOptions: {
                            styles: [{
                                color: location.id == {{ $user_id }} ?
                                    '#FF0000' : '#0000FF'
                            }]
                        },
                    }).addTo(map);

                    const routingControlContainer = control.getContainer();
                    routingControlContainer.style.width = '40%';

                    $('#userTableBody').append(`
                    <tr>
                        <td>${index + 1}</td>
                        <td>You</td>
                        <td>${location.cust_phone_number ? "Order" : "Daily"}</td>
                        <td>${location.user_lat}</td>
                        <td>${location.user_long}</td>
                        <td>${location.cust_phone_number || '-'}</td>
                        <td>${location.cust_address}</td>
                        <td>${location.add_information || '-'}</td>
                        <td>${location.cust_lat}</td>
                        <td>${location.cust_long}</td>
                    </tr>
                `);
                });
            } catch (error) {
                console.error('Error fetching user data or getting location:', error);
            }
        };


        // function sendLocationToServer() {
        // navigator.geolocation.getCurrentPosition(function(position) {
        // fetch("{{ route('user.update_lat_long') }}", {
        //         method: 'POST',
        //         headers: {
        //             'Content-Type': 'application/json',
        //             'X-CSRF-TOKEN': '{{ csrf_token() }}'
        //         },
        //         body: JSON.stringify({
        //             latitude: position.coords.latitude,
        //             longitude: position.coords.longitude
        //         })
        //     })
        //     .then(response => response.json())
        //     .then(data => console.log(data))
        //     .catch(error => console.error('Error sending location to server:', error));
        // });
        // }

        updateMarkers();

        // setInterval(updateMarkers, 3000);

        // sendLocationToServer();

        // setInterval(sendLocationToServer, 3000);
    </script>

@endsection
