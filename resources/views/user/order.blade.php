@extends('layout.masterdash')

@section('title')
    Receive Order
@endsection

@section('content')
<div class="card">
    <div class="card-header">
        <div class="card-title">
            Order
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table gy-5">
                <thead>
                    <tr class="fs-6 text-muted">
                        <th>No</th>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Address</th>
                        <th>Additional Information</th>
                        <th>Pickup At</th>
                        <th>Created At</th>
                        {{-- <th>Approved By</th> --}}
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($orders as $index => $order)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>{{ $order->name }}</td>
                        <td>{{ $order->phone }}</td>
                        <td>{{ $order->address }}</td>
                        <td>{{ $order->add_information }}</td>
                        <td>{{ $order->pick_up }}</td>
                        <td>{{ $order->created_at }}</td>
                        {{-- <td>{{ $order->approve_by }}</td> --}}
                        <td class="d-flex flex-column flex-sm-row">
                            @if($order->status < 3)
                            <form class="m-2" id="confirmForm{{ $order->id }}" action="{{ route('order.update_status', ['orderId' => $order->id]) }}" method="POST">
                                @csrf
                                {{-- @method('POST') --}}
                                <input type="hidden" name="status" value="{{ $order->status }}">

                                    <button type="submit" class="btn btn-primary approve-btn">
                                        @if($order->status == 0)
                                            Approve?
                                        @elseif($order->status == 1)
                                            Send Location?
                                        @else
                                            Finish?
                                        @endif
                                    </button>
                                </form>

                            <form class="m-2" id="cancelConfirmation{{ $order->id }}" action="{{ route('order.cancel_status', ['orderId' => $order->id]) }}" method="POST">
                                @csrf
                                @method('POST')

                                @if ($order->status == 1 /*|| $order->status == 2*/)
                                <button type="button" class="btn btn-danger approve-btn" onclick="cancelConfirmation('{{ $order->name }}', '{{ $order->id }}')">
                                    Cancel
                                </button>

                                @endif

                            </form>
                            @elseif($order->status == 3)
                                <button disabled class="btn btn-secondary">Waiting approval customer</button>
                            @else
                                Delivered
                            @endif

                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    function showConfirmation(orderName, id) {
        Swal.fire({
            title: 'Are you sure?',
            text: `Do you want to approve the order for ${orderName}?`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, approve it!'
        }).then((result) => {
            if (result.isConfirmed) {
                document.getElementById('confirmForm' + id).submit();
            }
        });
    }

    function cancelConfirmation(orderName, id) {
        Swal.fire({
            title: 'Are you sure?',
            text: `Do you want to cancel the order for ${orderName}?`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, cancel it!'
        }).then((result) => {
            if (result.isConfirmed) {
                document.getElementById('cancelConfirmation' + id).submit();
            }
        });
    }
</script>
@endsection
