@extends('layout.masterauth')
@section('title')
    Register
@endsection
@section('content')
<style>
    /* CSS for responsiveness */
    @media (max-width: 1199px) {
        .w-xl-800px {
            width: 100%;
        }
    }

    @media (max-width: 767px) {
        .h-300px {
            height: auto;
            max-width: 100%;
        }
    }

    /* Custom CSS for the eye icon */
    .password-toggle {
        position: relative;
    }
    .password-toggle input {
        padding-right: 2.5rem;
    }
    .password-toggle .toggle-icon {
        position: absolute;
        top: 70%;
        right: 1rem;
        transform: translateY(-50%);
        cursor: pointer;
    }
</style>
    <div class="d-flex flex-column flex-root">
        <!--begin::Authentication - Register -->
        <div class="d-flex flex-column flex-lg-row flex-column-fluid">
            <!--begin::Aside-->
            <div class="d-flex flex-column flex-lg-row-auto w-xl-800px positon-xl-relative">
                <!--begin::Wrapper-->
                <div class="d-flex flex-column position-xl-fixed top-0 bottom-0 w-xl-800px scroll-y">
                    <!--begin::Content-->
                    <div class="d-flex flex-row-fluid flex-column text-center p-10 pt-lg-20">
                        <!--begin::Logo-->
                        <a href="/" class="m-auto">
                            <img alt="Logo" src="{{ asset('media/image 2.png') }}" class="h-300px" />
                        </a>
                        <!--end::Logo-->
                    </div>
                    <!--end::Content-->
                </div>
                <!--end::Wrapper-->
            </div>
            <!--end::Aside-->
            <!--begin::Body-->
            <div class="d-flex bg-secondary flex-column flex-lg-row-fluid py-10">
                <!--begin::Content-->
                <div class="d-flex flex-center flex-column flex-column-fluid">
                    <!--begin::Wrapper-->
                    <div class="w-lg-500px p-10 p-lg-15 mx-auto">
                        <!--begin::Form-->
                        <form class="form w-100" novalidate="novalidate"
                            action="{{ route('register') }}" method="POST">
                            @csrf
                            <!--begin::Heading-->
                            <div class="text-center mb-10">
                                <!--begin::Logo-->
                                <a href="/" class="py-9 mb-5">
                                    <img alt="Logo" src="{{ asset('media/Logo.png') }}" class="h-75px" />
                                </a>
                                <!--end::Logo-->
                                <!--begin::Title-->
                                <h2 class="text-dark mb-3"><span style="color: #86DA36">Cikarang Smart</span> <span style="color: #3A9106">Organization Waste</span></h2>
                                <h2 class="text-dark mb-3">Register to CSOW Fleet</h2>
                                <!--end::Title-->
                                <!--begin::Link-->
                                <div class="text-black-400 fw-bold fs-4">Already have an account?
                                    <a href="{{ route('login') }}" class="link-primary fw-bolder">Sign In</a>
                                </div>
                                <!--end::Link-->
                            </div>

                            <!--end::Heading-->
                            <!--begin::Input group-->
                            <div class="fv-row mb-10">
                                <!--begin::Label-->
                                <label class="form-label fs-6 fw-bolder text-dark">Name</label>
                                <!--end::Label-->
                                <!--begin::Input-->
                                <input class="form-control form-control-lg form-control-solid" type="text" name="name"
                                    autocomplete="off" placeholder="name" />
                                <!--end::Input-->
                            </div>
                            <!--end::Input group-->
                            <!--begin::Input group-->
                            <div class="fv-row mb-10">
                                <!--begin::Label-->
                                <label class="form-label fs-6 fw-bolder text-dark">Email</label>
                                <!--end::Label-->
                                <!--begin::Input-->
                                <input class="form-control form-control-lg form-control-solid" type="email" name="email"
                                    autocomplete="off" placeholder="name@example.com" />
                                <!--end::Input-->
                            </div>
                            <!--end::Input group-->
                            <!--begin::Input group-->
                            <div class="fv-row mb-10 password-toggle">
                                <!--begin::Wrapper-->
                                <div class="d-flex flex-stack mb-2">
                                    <!--begin::Label-->
                                    <label class="form-label fw-bolder text-dark fs-6 mb-0">Password</label>
                                    <!--end::Label-->
                                </div>
                                <!--end::Wrapper-->
                                <!--begin::Input-->
                                <input class="form-control form-control-lg form-control-solid" type="password"
                                    name="password" id="password" autocomplete="off" />
                                <!--end::Input-->
                                <!--begin::Toggle icon-->
                                <span class="toggle-icon" onclick="togglePasswordVisibility()">
                                    <i id="eye-icon" class="bi bi-eye-slash"></i>
                                </span>
                                <!--end::Toggle icon-->
                            </div>
                            <!--end::Input group-->
                            <!--begin::Actions-->
                            <div class="text-center">
                                <!--begin::Submit button-->
                                <button type="submit" id="kt_sign_in_submit" class="btn btn-lg btn-primary w-50 mb-5">
                                    <span class="indicator-label">Sign Up</span>
                                    <span class="indicator-progress">Please wait...
                                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                </button>
                                <!--end::Submit button-->
                            </div>
                            <!--end::Actions-->
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Wrapper-->
                </div>
                <!--end::Content-->
                <!--begin::Footer-->
                <div class="d-flex flex-center flex-wrap fs-6 p-5 pb-0">
                    <!--begin::Links-->
                    <div class="d-flex flex-center fw-bold fs-6 p-1 bg-dark rounded">
                        <a href="/" class="text-muted text-hover-primary px-2">Back</a>
                    </div>
                    <!--end::Links-->
                </div>
                <!--end::Footer-->
            </div>
            <!--end::Body-->
        </div>
        <!--end::Authentication - Register-->
    </div>

    <!-- Success Popup -->
    <div class="modal fade" id="successPopup" tabindex="-1" aria-labelledby="successPopupLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <div class="my-3">
                        <i class="fas fa-check-circle fa-5x text-success"></i>
                    </div>
                    <h3 class="modal-title" id="successPopupLabel">Registration Successful!</h3>
                    <p>You have successfully registered.</p>
                    <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Error Popup -->
    <div class="modal fade" id="errorPopup" tabindex="-1" aria-labelledby="errorPopupLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <div class="my-3">
                        <i class="fas fa-exclamation-circle fa-5x text-danger"></i>
                    </div>
                    <h3 class="modal-title" id="errorPopupLabel">Registration Failed</h3>
                    <p>{{ Session::get('error') }}</p>
                    <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.min.js"></script>
    <script>
        // Show success popup after successful registration
        $(document).ready(function() {
            @if (Session::has('success'))
                $('#successPopup').modal('show');
            @endif

            @if (Session::has('error'))
                $('#errorPopup').modal('show');
            @endif
        });

        // Toggle password visibility
        function togglePasswordVisibility() {
            const passwordInput = document.getElementById('password');
            const eyeIcon = document.getElementById('eye-icon');
            if (passwordInput.type === 'password') {
                passwordInput.type = 'text';
                eyeIcon.classList.remove('bi-eye-slash');
                eyeIcon.classList.add('bi-eye');
            } else {
                passwordInput.type = 'password';
                eyeIcon.classList.remove('bi-eye');
                eyeIcon.classList.add('bi-eye-slash');
            }
        }
    </script>
@endsection
