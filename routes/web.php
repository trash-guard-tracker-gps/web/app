<?php

use App\Events\PinLocation;
use App\Events\testEvent;
use App\Events\testingEvent;
use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\CustomerOrderController;
use App\Http\Controllers\GpsController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\SensorSampahController;
use App\Models\CustomerLogin;
use App\Models\Order;
use Illuminate\Support\Facades\Route;

// Public Routes
Route::get('/', function () {
    return view('user.dashboard');
})->name("home");

Route::get('/simpan/{nilailevel}/{nilaistatus}', [SensorSampahController::class, 'simpansensor']);

// Authenticated Routes
Route::middleware(['auth', 'verified'])->group(function () {
    Route::get('/dashboard', function () {
        return view('user.dashboard');
    })->name('dashboard');

    Route::post("/user/update-lat-long", [UserController::class, 'update_lat_long'])->name('user.update_lat_long');

    Route::get('/sensor', [SensorSampahController::class, 'index'])->name('sensor');
    Route::get('/levelsensor', [SensorSampahController::class, 'levelsensor'])->name('levelsensor');
    Route::get('/statussensor', [SensorSampahController::class, 'statussensor'])->name('statussensor');
    Route::get('/alamatsensor', [SensorSampahController::class, 'alamatsensor'])->name('alamatsensor');

    Route::get('/gps', [GpsController::class, 'index'])->name('gps');

    Route::get('/customer', [CustomerController::class, 'index'])->name('customer.index');
    Route::post('/customer/{userId}', [CustomerController::class, 'delete_by_id'])->name('customer.delete_by_id');

    Route::get('/order', [OrderController::class, 'index'])->name('order.index');
    Route::post('/order/status/${orderId}', [OrderController::class, 'update_status'])->name('order.update_status');
    Route::post('/order/cancel-status/${orderId}', [OrderController::class, 'cancel_status'])->name('order.cancel_status');

    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/send-event', function () {
    event(new PinLocation(12, 15));

    return "done";
});

Route::post("", function () {

});


// Authentication Routes
require __DIR__ . '/auth.php';
