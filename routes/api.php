<?php

use App\Events\CustomerLocation;
use App\Events\PinLocation;
use App\Events\UserLocation;
use App\Http\Controllers\AuthCustomer;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\GpsController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\SensorSampahController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:sanctum');

Route::prefix('auth')->group(function () {
    Route::post('login', [AuthCustomer::class, 'login'])->name('auth_customer.login');
    Route::post('register', [AuthCustomer::class, 'register'])->name('auth_customer.register');
    Route::post('forgot-password', [AuthCustomer::class, 'forgot_password'])->name('auth_customer.forgot_password');
    Route::post('verify-otp/{customerId}', [AuthCustomer::class, 'verify_otp'])->name('auth_customer.verify_otp');
});

/* Ini buat customer atau mobile app untuk authentication */
Route::prefix('customer')->group(function () {
    Route::post('update/{customerId}', [CustomerController::class, 'update'])->name('customer.update');
    Route::post('get-by-id/{customerId}', [CustomerController::class, 'get_by_id'])->name('customer.get_by_id');
    Route::post('get-by-phone', [CustomerController::class, 'get_by_phone'])->name('customer.get_by_phone');
});

/* Ini buat customer untuk order pickup */
Route::prefix('order')->group(function () {
    Route::post('create', [OrderController::class, 'create'])->name('order.create');
    Route::post('update/{orderId}', [OrderController::class, 'update'])->name('order.update');
});

/* ini sebenernya gk di butuhin sih */
Route::prefix('user')->group(function () {
    Route::get('get', [UserController::class, 'get'])->name('user.get');
});

/* Ini buat web untuk view gps page */
Route::prefix('gps')->group(function () {
    Route::get('get', [GpsController::class, 'get'])->name('gps.get');
});

/* Ini buat web untuk view sensor page */
Route::prefix('sensor')->group(function () {
    Route::get('/levelsensor', [SensorSampahController::class, 'getLevelSensor'])->name('sensor.level_sensor');
    Route::get('/statussensor', [SensorSampahController::class, 'getStatusSensor'])->name('sensor.status_sensor');
    Route::get('/alamatsensor', [SensorSampahController::class, 'getAlamatSensor'])->name('sensor.alamat_sensor');
});

Route::post('location', function (Request $request) {
    $lat = (int) $request->input('lat');
    $long = (int) $request->input('long');
    $type = $request->input('type');

    $types = ['user', 'customer'];

    if (!in_array($type, $types)) {
        return response()->json(["message" => "Types only for user and customer"]);
    }

    event($type === "user" ? new UserLocation($lat, $long) : new CustomerLocation($lat, $long));


    return response()->json(['message' => 'Send lat and long successfully']);
})->name('location.post');


// Route::post('location', function (Request $request) {
//     $lat = (int) $request->input('lat');
//     $long = (int) $request->input('long');

//     event(new PinLocation($lat, $long));

//     return response()->json(['message' => 'Send lat and long successfully']);
// });
