<?php

use Illuminate\Foundation\Application;
use Illuminate\Foundation\Configuration\Exceptions;
use Illuminate\Foundation\Configuration\Middleware;
use App\Http\Middleware\Cors; // Import your Cors middleware

return Application::configure(basePath: dirname(__DIR__))
    ->withRouting(
        web: __DIR__.'/../routes/web.php',
        api: __DIR__.'/../routes/api.php',
        commands: __DIR__.'/../routes/console.php',
        channels: __DIR__.'/../routes/channels.php',
        health: '/up',
    )
    ->withMiddleware(function (Middleware $middleware) {

        // $middleware->append(Cors::class);
        $middleware->alias([
            "check_user_count" => \App\Http\Middleware\CheckUserCount::class,
            "coordinate_realtime" => \App\Http\Middleware\CoordinateRealtime::class
        ]);
    })
    ->withExceptions(function (Exceptions $exceptions) {
        //
    })->create();
