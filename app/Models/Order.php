<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'phone',
        'customer_id',
        'approve_by',
        'address',
        'add_information',
        'pick_up',
        'status',
    ];

}
