<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'lat',
        'long'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    /**
     * Validation rules.
     *
     * @var array<string, string>
     */
    public static $rules = [
        'name' => 'required|string|max:255',
        'email' => 'required|email|unique:users,email',
        'password' => 'required|string|min:8',
    ];

    /**
     * Retrieve a user by their email.
     *
     * @param string $email
     * @return \App\Models\User|null
     */
    public static function getByEmail($email)
    {
        return self::where('email', $email)->first();
    }

    /**
     * Define the user's posts relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    // public function posts()
    // {
    //     return $this->hasMany(Post::class);
    // }

    /**
     * Accessor for the user's full name.
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->name;
    }

    /**
     * Mutator for hashing the user's password.
     *
     * @param string $password
     * @return void
     */
    // public function setPasswordAttribute($password)
    // {
    //     $this->attributes['password'] = bcrypt($password);
    // }

    /**
     * Boot method to register events and observers.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        // Event to perform actions when a user is created
        static::created(function ($user) {
            // Example: send a welcome email to the user
            // Mail::to($user->email)->send(new WelcomeEmail($user));
        });

        // Observer to perform actions when a user is deleted
        static::deleting(function ($user) {
            // Example: delete all posts associated with the user
            // $user->posts()->delete();
        });
    }
}
