<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SensorSampah extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $table = 'sensor_sampah';
    protected $fillable = ['sensor','status','alamat'];
}

