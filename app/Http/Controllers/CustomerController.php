<?php

namespace App\Http\Controllers;


use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

/* Semuanya ini untuk function data customer api */
class CustomerController extends Controller
{
    public function index()
    {
        $customers = Customer::all();

        return view('user.customer')->with('customers', $customers);
    }

/* Semuanya ini untuk function update data customer di mobile khususnya ganti password dan information akun */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'string',
            'phone' => 'string',
            'address' => 'string',
            'password' => 'string',
            'otp' => 'int',
            'lat' => 'numeric',
            'long' => 'numeric',

        ];

        try {

            if (!$id) {
                return response()->json(['message' => 'Customer id should be fill'], 401);
            }

            $request->validate($rules);
            $validatedData = $request->only(array_keys($rules));

            if ($request->filled('password')) {
                $validatedData['password'] = Hash::make($request->password);
            }

            $customer = Customer::findOrFail($id);
            $customer->update($validatedData);

            return response()->json(['message' => 'Customer updated successfully']);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

/* function ini di butuhin buat bikin logic di home mobile app buat ngambil data customer  */
    public function get_by_id($id)
    {
        if (!$id) {
            return response()->json(['error' => 'Id should be filled'], 404);
        }

        try {
   $customers = DB::select("SELECT c.*, o.status as order_status, o.id as orderId, u.name as officer_name, c.lat as cust_lat, c.long as cust_long, u.lat as officer_lat, u.long as officer_long FROM customers c INNER JOIN orders o ON c.id = o.customer_id LEFT JOIN users u ON u.id = o.approve_by WHERE c.id = :id AND (o.status != 0 OR u.name IS NOT NULL) ORDER BY o.id DESC LIMIT 1", ['id' => $id]);

            return response()->json(['status' => 'success', 'result' => $customers]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }


/* function ini di butuhin buat forgot password di mobile app */
    public function get_by_phone(Request $request)
    {
        try {
            $data = $request->validate([
                "phone" => "required|string"
            ]);

            $customer = Customer::where('phone', $data['phone'])->first();

            if (!$customer) {
                return response()->json(['error' => 'Customer not found'], 404);
            }

            return response()->json(['status' => 'success', 'result' => $customer]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }


/* function ini di butuhin buat delete data customer di web */
    public function delete_by_id($id)
    {
        $customer_regist = Customer::findOrFail($id);
        $customer_regist->delete();

        return redirect()->back()->with('success', 'Customer registration deleted successfully.');
    }
}
