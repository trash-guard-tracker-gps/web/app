<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SensorSampah;


/* Semuanya ini untuk function data sensor api untuk web */
class SensorSampahController extends Controller
{
    public function index()
    {
        //baca, ambil, isi table
        $monitor = SensorSampah::all();
        return view('user.sensor', ['title' => 'Sensor Volume', 'sensormonitor'=>$monitor]);
    }
    public function levelsensor()
    {
        //baca, ambil, isi table
        $monitor = SensorSampah::all();
        return view('user.levelsensor', ['title' => 'Sensor Volume', 'sensormonitor'=>$monitor]);
    }
    public function statussensor()
    {
        //baca, ambil, isi table
        $monitor = SensorSampah::all();
        return view('user.statussensor', ['title' => 'Sensor Volume', 'sensormonitor'=>$monitor]);
    }
    public function alamatsensor()
    {
        //baca, ambil, isi table
        $monitor = SensorSampah::all();
        return view('user.alamatsensor', ['title' => 'Sensor Volume', 'sensormonitor'=>$monitor]);
    }

    public function simpansensor()
    {
        SensorSampah::where('id', '1')->update(['sensor'=> request()->nilailevel, 'status'=>request()->nilaistatus]);
    }

}
