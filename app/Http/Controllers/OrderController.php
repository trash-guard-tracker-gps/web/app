<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/* Semuanya ini untuk function data order api untuk web dan juga mobile */
class OrderController extends Controller
{
/* Ini khusus nampilin order page di web */
    public function index()
    {
        $orders = DB::select("SELECT o.id, o.name, o.phone, u.name as approve_by, o.address, o.add_information, o.pick_up, o.status, o.created_at as created_at FROM orders o INNER JOIN customers c ON o.customer_id = c.id LEFT JOIN users u ON u.id = o.approve_by");

        return view('user.order')->with('orders', $orders);
    }

/* Ini function buat change status di web */
    public function update_status(Request $request, $id)
    {
        $userId = session('user_id');

        $request->validate([
            'status' => 'required|integer',
        ]);

        $status = $request->input('status');

        if ($status == 0) {
            Order::where('id', $id)->update(['approve_by' => $userId, 'status' => '1']);
        } elseif ($status == 1) {
            Order::where('id', $id)->update(['status' => '2']);
        } elseif ($status == 2) {
            Order::where('id', $id)->update(['status' => '3']);
        } else {
            Order::where('id', $id)->update(['status' => '4']);
        }

        return redirect()->back()->with('status', 'success');
    }

/* Ini function buat cancel status di web */
    public function cancel_status($id)
    {
        Order::where('id', $id)->update(['status' => '0', 'approve_by' => 0]);

        return redirect()->back()->with('status', 'success');
    }

/* Ini function buat membuat data order di mobile */
    public function create(Request $request)
    {

        $rules = [
            'name' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'customer_id' => 'required',
            'pick_up' => 'required|date',
            'add_information' => 'nullable',
        ];

        try {

            $request->validate($rules);
            $validatedData = $request->only(array_keys($rules));

            $order = Order::create([
                'name' => $validatedData['name'],
                'phone' => $validatedData['phone'],
                'address' => $validatedData['address'],
                'customer_id' => $validatedData['customer_id'],
                'approve_by' => 0,
                'add_information' => $validatedData['add_information'],
                'pick_up' => $validatedData['pick_up']
            ]);


            return response()->json(['message' => 'Order created successfully', 'result' => $order]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

/* Ini function buat mengganti status order di mobile */
    public function update(Request $request, $id)
    {
        $rules = [
            'customer_id' => 'nullable|exists:customers,id',
            'approve_by' => 'nullable',
            'address' => 'nullable',
            'add_information' => 'nullable',
            'pick_up' => 'nullable',
            'status' => 'nullable',
        ];

        try {
            $request->validate($rules);

            $validatedData = $request->only(array_keys($rules));

            if (!$id) {
                return response()->json(['message' => 'Order id should be fill'], 401);
            }

            $order = Order::findOrFail($id);

            foreach ($validatedData as $key => $value) {
                if ($value !== null) {
                    $order->$key = $value;
                }
            }

            $order->save();

            return response()->json(['message' => 'Order updated successfully', 'result' => $order]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

}
