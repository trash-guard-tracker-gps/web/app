<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\AuthCustomerRequest;
use App\Mail\ForgotPasswordMail;
use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;


/* Semuanya ini untuk function authentication customer api */
class AuthCustomer extends Controller
{
    public function login(Request $request)
    {
        try {
            $loginCustomerRequest = new AuthCustomerRequest();

            ['phone' => $phone, 'password' => $password] = $request->validate($loginCustomerRequest->rules()[0]);

            $customer = DB::select("
                SELECT * FROM customers
                WHERE phone = :phone
                LIMIT 1
            ", ['phone' => $phone]);

            if (!empty($customer) && Hash::check($password, $customer[0]->password)) {
                $latestOrderStatus = DB::select("
                SELECT o.id as orderId, o.status, u.lat as user_lat, u.long as user_long, u.name as user_name
                FROM orders o
                LEFT JOIN users u ON u.id = o.approve_by
                WHERE o.customer_id = :customer_id
                ORDER BY o.id DESC
                LIMIT 1;
                ", ['customer_id' => $customer[0]->id]);

                if ($customer[0]->id && $customer[0]->name && $customer[0]->phone && $customer[0]->email && $customer[0]->address) {
                    $customerData = [
                        "id" => $customer[0]->id,
                        "name" => $customer[0]->name,
                        "phone" => $customer[0]->phone,
                        "email" => $customer[0]->email,
                        "address" => $customer[0]->address,
                                  "cust_lat" => $customer[0]->lat,
                        "cust_long" => $customer[0]->long,
                        "status" => !empty($latestOrderStatus) ? $latestOrderStatus[0]->status : "5",
                        "orderId" => !empty($latestOrderStatus) ? $latestOrderStatus[0]->orderId : 0,
                        "officer_name" => !empty($latestOrderStatus) ? $latestOrderStatus[0]->user_name : "",
                        "officer_lat" => !empty($latestOrderStatus) ? $latestOrderStatus[0]->user_lat : 0,
                        "officer_long" => !empty($latestOrderStatus) ? $latestOrderStatus[0]->user_long : 0
                    ];

                    return response()->json([
                        "status" => "success",
                        "result" => $customerData,
                    ]);
                }
            }

            return response()->json(["error" => "Invalid phone or password"], 401);
        } catch (\Exception $e) {
            return response()->json(["error" => $e->getMessage()], 500);
        }
    }


    public function register(Request $request)
    {
        try {
            $registerCustomerRequest = new AuthCustomerRequest();

            ['name' => $name, 'phone' => $phone, 'address' => $address, 'email' => $email, 'password' => $password] = $request->validate($registerCustomerRequest->rules()[1]);

            $existingPhone = Customer::where('phone', $phone)->exists();
            if ($existingPhone) {
                throw new \Exception("Phone number already exists.");
            }

            $existingEmail = Customer::where('email', $email)->exists();
            if ($existingEmail) {
                throw new \Exception("Email already exists.");
            }

            $result = Customer::create([
                'name' => $name,
                'phone' => $phone,
                'address' => $address,
                'email' => $email,
                'password' => Hash::make($password),
            ]);

            return response()->json(["status" => "success", "result" => $result]);
        } catch (\Exception $e) {
            return response()->json(["error" => $e->getMessage()], 500);
        }
    }


    public function forgot_password(Request $request)
    {
        try {
            $data = $request->validate(["phone" => "required|string"]);

            $customer = Customer::where('phone', $data['phone'])->first();

            if (!$customer) {
                return response()->json(["error" => "Phone not found"], 404);
            }

            $otp = mt_rand(1000, 9999);

            $customer->otp = $otp;
            $customer->save();

            Mail::to($customer->email)->send(new ForgotPasswordMail($otp));

            return response()->json(["status" => "success", "message" => "OTP sent to your email", "result" => $customer]);
        } catch (\Exception $e) {
            return response()->json(["error" => $e->getMessage()], 500);
        }
    }

    public function verify_otp(Request $request, $id)
    {
        try {
            $data = $request->validate([
                'otp' => 'required|integer',
            ]);

            if (!$id) {
                return response()->json(['message' => 'Customer id should be filled'], 401);
            }

            $customer = Customer::find($id);

            if (!$customer) {
                return response()->json(['error' => 'Customer not found'], 404);
            }

            if ($customer->otp == $data['otp']) {
                $customer->otp = 0;
                $customer->save();

                return response()->json(["status" => "success", "message" => "OTP verified successfully"]);
            } else {
                return response()->json(["error" => "Invalid OTP"], 401);
            }
        } catch (\Exception $e) {
            return response()->json(["error" => $e->getMessage()], 500);
        }
    }
}

