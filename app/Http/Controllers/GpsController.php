<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/* Semuanya ini untuk function data gps api untuk web */
class GpsController extends Controller
{

/* Ini buat nampilin data di gps sma nampilin page gps */
    public function index(Request $request)
    {
        $user_id = session()->get('user_id');
        $user = User::find($user_id);

        $users = User::all();

        $users = DB::select("SELECT u.id, u.name, u.lat as user_lat, u.long as user_long, c.lat as cust_lat, c.long as cust_long, o.phone as cust_phone_number, o.address as cust_address, o.add_information FROM users u INNER JOIN orders o ON u.id = o.approve_by INNER JOIN customers c ON o.customer_id = c.id WHERE o.status != '0' AND o.status != '4'");

        return view('user.gps', compact('users', 'user_id', 'user'));
    }

/* Ini function tambahan logic buat gps page */
    public function get()
    {
        $user = User::get();

        $users = DB::select("
        SELECT u.id, u.name, u.lat as user_lat, u.long as user_long, c.name as cust_name,
               c.lat as cust_lat, c.long as cust_long, o.phone as cust_phone_number,
               o.address as cust_address, o.add_information, 0 as role
        FROM users u
        INNER JOIN orders o ON u.id = o.approve_by
        INNER JOIN customers c ON o.customer_id = c.id
        WHERE o.status != '0' AND o.status != '4'
    ");

        $locations = [
            [-6.34608588547509, 107.01002090272812, "M236+H5H, RT.003/RW.002, Sumur Batu, Kec. Bantar Gebang, Kota Bks, Jawa Barat 17154"], // Rumah (Sumur Batu)
            [-6.351955104577968, 107.01888023893666, "Burangkeng, Kec. Setu, Kabupaten Bekasi, Jawa Barat"], // Jln Pangkalan II Burangkeng
            [-6.33558579801908, 107.01921977080939, "M279+PPG, Burangkeng, Kec. Setu, Kabupaten Bekasi, Jawa Barat 17320"], // Jln Cinyongsong
            // [-6.285786776006559, 107.07065452425925, "P37C+H5W, Jl. Raya Setu, Cibuntu, Kec. Cibitung, Kabupaten Bekasi, Jawa Barat 17530"], // Jln Raya Setu
            // [-6.283514771796865, 107.10550935428088, "Sukadanau, Kec. Cikarang Bar., Kabupaten Bekasi, Jawa Barat"], // Jln Inpeksi Kalimalang
            // [-6.296803772235656, 107.03069074130369, "Jalan Raya Mustikajaya-Legenda No.58, Mustikajaya, Bekasi Timur, RT.002/RW.011, Mustika Jaya, Kec. Mustika Jaya, Kota Bks, Jawa Barat 17158"], // Jln Mustika Jaya
            // [-6.278775475445694, 107.04828290638837, "Jl. Celebration Boulevard, Lambangjaya, Kec. Tambun Sel., Kabupaten Bekasi, Jawa Barat 17510"], // Jln Boulevard Grand Wisata
            // [-6.269578905717856, 107.0874071794103, "P3JP+7P9, Jl. Teuku Umar, Raya Cibitung-Bekasi, Telaga Asih, Kec. Cikarang Bar., Kabupaten Bekasi, Jawa Barat 17530"], // Jln Raya Teuku Umar
            // [-6.265766444952761, 107.13574527338525, "Jl. Raya Fatahillah No.89, Kalijaya, Kec. Cikarang Bar., Kabupaten Bekasi, Jawa Barat 17352"], // SPBU 34.1750.07(Jl Fatahilah No.89 Kec Cikarang Barat)
            // [-6.258875474646793, 107.14640159475493, "Jl. RE. Martadinata No.95, Cikarang Kota, Kec. Cikarang Utara, Kabupaten Bekasi, Jawa Barat 17530"], // Jln RE Martadinata
            // [-6.262673608774173, 107.17497168311716, "Jalan Urip Sumoharjo No.18, Cikarang Utara, Tanjungsari, Bekasi, Kabupaten Bekasi, Jawa Barat 17530"], // Jln Jend Urip Sumoharjo
            // [-6.267432658366451, 107.22868778126377, "P6MH+2GR, Karangsambung, Kec. Kedungwaringin, Kabupaten Bekasi, Jawa Barat 17540"], // Jln Raya Rengasbandung
            // [-6.2692577074380225, 107.25723039037567, "Bojongsari, Kec. Kedungwaringin, Kabupaten Bekasi, Jawa Barat 17540"], // Jln Raya Bojong Sari
            // [-6.264552134170251, 107.2730262288962, "Perum kedungwaringin No.b11/22, Kedungwaringin, Kec. Kedungwaringin, Kabupaten Bekasi, Jawa Barat 17540"], // Perumahan Kedung Waringin
            // [-6.269201315303258, 107.27054438252755, "Kedungwaringin, Kec. Kedungwaringin, Kabupaten Bekasi, Jawa Barat 17540"], // Jln Kedung Waringin
            // [-6.267389983329329, 107.22870921921175, "Jl. Raya Rengas Bandung No.82, Tanjungbaru, Kec. Cikarang Tim., Kabupaten Bekasi, Jawa Barat 17530"], // Jln Raya Rengasbandung
            // [-6.270549259382987, 107.18954161565992, "P5HQ+QJ8, Karangsari, Kec. Cikarang Tim., Kabupaten Bekasi, Jawa Barat 17530"], // Jln Urip Sumoharjo
            // [-6.302478397806049, 107.14454683656501, "Jl. Raya Industri No.18, Pasirsari, Cikarang Sel., Kabupaten Bekasi, Jawa Barat 17530"], // Jln Inpeksi Kalimalang
            // [-6.314877503560377, 107.02557229660889, "RT.004/RW.007, Cimuning, Kec. Mustika Jaya, Kota Bks, Jawa Barat"], // Jln Mustika Jaya
            // [-6.301614617274188, 107.06664650942855, "Jl. Raya Setu, Cibuntu, Kec. Cibitung, Kabupaten Bekasi, Jawa Barat 17520"], // Jln Raya Setu
            // [-6.353906930704609, 107.02208122544523, "J2WC+FM5, Gg. Wirjo, Burangkeng, Kec. Setu, Kabupaten Bekasi, Jawa Barat 17320"], // TPA Burangkeng
            // [-6.462562, 106.914157], // Rumah
        ];

        $additional_data = [];

        /* 0 = Driver, 1 = Destionation Pickup trash */

        foreach ($locations as $location) {
            $additional_data[] = (object)[
                "id" => "",
                "name" => "",
                "user_lat" => $user[0]->lat,
                "user_long" => $user[0]->long,
                "cust_name" => "",
                "cust_lat" => $location[0],
                "cust_long" => $location[1],
                "cust_phone_number" => "",
                "cust_address" => $location[2],
                "add_information" => "",
                "role" => 1
            ];
        }


        $users = array_merge($users, $additional_data);

        foreach ($users as &$user) {
            $user->distance = $this->calculateDistance($user->user_lat, $user->user_long, $user->cust_lat, $user->cust_long);
        }

        usort($users, function ($a, $b) {
            return $a->distance - $b->distance;
        });

        return response()->json(["status" => "success", "result" => $users]);
    }

/* Ini logic function buat sorting jarak terdekat petugas */
    private function calculateDistance($lat1, $lon1, $lat2, $lon2)
    {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        return ($miles * 1.609344);
    }
}
