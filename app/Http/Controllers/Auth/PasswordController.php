<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules\Password;

class PasswordController extends Controller
{
    /**
     * Update the user's password.
     */
    public function update(Request $request): RedirectResponse
    {
        try {
            $validated = $request->validate([
                'email' => ['required', 'email'], // Validate the email address
                'password' => ['required', Password::defaults(), 'confirmed'], // Validate the new password
            ]);

            $user = User::where('email', $validated['email'])->first();

            if (!$user) {
                throw new \Exception("User not found.");
            }

            $user->update([
                'password' => Hash::make($validated['password']),
            ]);

            session()->flash('status', 'Password updated successfully.');

            return redirect()->route('login')->with('status', 'Password updated successfully.');
        } catch (\Exception $e) {
            return redirect()->back()->withErrors([$e->getMessage()]);
        }
    }

}
