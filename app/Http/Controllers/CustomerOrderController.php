<?php

namespace App\Http\Controllers;
use App\Models\CustomerOrder;
use Illuminate\Http\Request;

class CustomerOrderController extends Controller
{
    public function index()
    {
        $customer_orders = CustomerOrder::all();

        return view('user.order', compact('customer_orders'));
    }

    public function update_status(Request $request, $userId)
    {
        $customer_order = CustomerOrder::find($userId);

        if (!$customer_order) {
            return redirect()->back()->with('error', 'Order not found.');
        }

        $customer_order->status = '1';
        $customer_order->save();

        return redirect()->back()->with('success', 'Order status updated successfully.');
    }
}
