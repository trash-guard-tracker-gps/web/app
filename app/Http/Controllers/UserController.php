<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function update_lat_long(Request $request)
    {
        try {
            $data = $request->validate([
                'latitude' => 'required|numeric',
                'longitude' => 'required|numeric',
            ]);

            $sessionId = session()->get('user_id');

            $user = User::where('id', $sessionId)->first();

            if (!$user) {
                return response()->json(["error" => "User not found"], 404);
            }

            $user->update([
                'lat' => $data['latitude'],
                'long' => $data['longitude'],
            ]);

            return response()->json(["status" => "success", "message" => "Latitude and longitude updated successfully"]);
        } catch (\Exception $e) {
            return response()->json(["error" => $e->getMessage()], 500);
        }
    }

    public function get(Request $request)
    {
        try {
            $user = User::get();

            return response()->json([
                "status" => "success",
                "name" => $user[0]->name,
                "latitude" => $user[0]->lat,
                "longitude" => $user[0]->long,
            ]);
        } catch (\Exception $e) {
            return response()->json(["error" => $e->getMessage()], 500);
        }
    }



}
