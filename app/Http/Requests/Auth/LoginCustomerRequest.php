<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class LoginCustomerRequest extends FormRequest
{

    public function rules(): array
    {
        return [
            'Phone' => ['required', 'string'],
            'Password' => ['required', 'string'],
        ];
    }

}
