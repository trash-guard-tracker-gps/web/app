<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class AuthCustomerRequest extends FormRequest
{

    public function rules(): array
    {
        return [
            [
                'phone' => ['required', 'string'],
                'password' => ['required', 'string'],
            ],
            [
                'name' => ['required', 'string'],
                'phone' => ['required', 'string'],
                'password' => ['required', 'string'],
                'address' => ['required', 'string'],
                'email' => ['required', 'string']
            ],
            [
                "email" => ["required", "string"]
            ]
        ];
    }

}
